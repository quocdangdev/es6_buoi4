// render todo
export let renderToDo = (list) => {
  let contentHTML = "";
  list.forEach((item, index) => {
    contentHTML += `<li>
        ${item}
        <span>
        <i onclick="xoaItem('${index}')" class="fa-solid fa-trash-can"></i>
        <i onclick="checkItem('${index}')" class="fa-solid fa-circle-check"></i>
        </span>
        </li>
        `;
  });
  document.getElementById("todo").innerHTML = contentHTML;
};
export let renderToDoCompleted = (list) => {
  let contentHTML = "";
  list.forEach((task, index) => {
    contentHTML += `<li>
        ${task}
        <span>
        <i onclick="xoaItemCompleted('${index}')" class="fa-solid fa-trash-can"></i>
        <i onclick="checkItem('${index}')" class="fa-solid fa-circle-check"></i>
        </span>
        </li>
        `;
  });
  document.getElementById("completed").innerHTML = contentHTML;
};

export let saveLocalStorage = (list) => {
  // biến đổi json ->
  let data = JSON.stringify(list);
  localStorage.setItem("DS", data);
};

export let sortZA = (list) => {
  list.sort((a, b) => {
    const nameA = a.toUpperCase(); // ignore upper and lowercase
    const nameB = b.toUpperCase(); // ignore upper and lowercase
    if (nameA > nameB) {
      return -1;
    }
    if (nameA < nameB) {
      return 1;
    }
    return 0;
  });
  saveLocalStorage(list);
  renderToDo(list);
};
export let sortAZ = (list) => {
  list.sort((a, b) => {
    const nameA = a.toUpperCase();
    const nameB = b.toUpperCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  });
  saveLocalStorage(list);
  renderToDo(list);
};
// export let sortAZCompleted = (list) => {
//   list.sort((a, b) => {
//     const nameA = a.toUpperCase();
//     const nameB = b.toUpperCase();
//     if (nameA < nameB) {
//       return -1;
//     }
//     if (nameA > nameB) {
//       return 1;
//     }
//     return 0;
//   });
//   saveLocalStorage(list);
//   renderToDoCompleted(list);
// };

// export let sortZACompleted = (list) => {
//   list.sort((a, b) => {
//     const nameA = a.toUpperCase();
//     const nameB = b.toUpperCase();
//     if (nameA > nameB) {
//       return -1;
//     }
//     if (nameA < nameB) {
//       return 1;
//     }
//     return 0;
//   });
//   saveLocalStorage(list);
//   renderToDoCompleted(list);
// };
